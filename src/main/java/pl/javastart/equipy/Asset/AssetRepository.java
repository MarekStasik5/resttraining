package pl.javastart.equipy.Asset;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AssetRepository extends JpaRepository<Asset, Long> {

	@Query("SELECT a FROM Asset a WHERE lower(a.name) LIKE lower(concat('%', :search, '%')) "
			+ "OR lower(a.serialNumber) LIKE lower(concat('%', :search, '%'))")
	List<Asset> findAllByNameOrSerialNumber(@Param("search") String search);

	Optional<Asset> findBySerialNumber(String serialNumber);
}
