package pl.javastart.equipy.Assignment;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import pl.javastart.equipy.Asset.Asset;
import pl.javastart.equipy.Asset.AssetRepository;
import pl.javastart.equipy.Exception.AssignmentAlreadyFinishedException;
import pl.javastart.equipy.Exception.AssignmentNotFoundException;
import pl.javastart.equipy.Exception.InvalidAssignmentException;
import pl.javastart.equipy.User.User;
import pl.javastart.equipy.User.UserReposiotry;

@Service
class AssignmentService {
	private AssignmentRepository assignmentRepository;
	private AssetRepository assetRepository;
	private UserReposiotry userRepository;

	public AssignmentService(AssignmentRepository assignmentRepository, AssetRepository assetRepository,
			UserReposiotry userRepository) {
		this.assignmentRepository = assignmentRepository;
		this.assetRepository = assetRepository;
		this.userRepository = userRepository;
	}

	AssignmentDto createAssignment(AssignmentDto assignmentDto) {
		Optional<Assignment> activeAssignmentForAsset = assignmentRepository
				.findByAsset_IdAndEndIsNull(assignmentDto.getAssetId());
		activeAssignmentForAsset.ifPresent((s) -> {
			throw new InvalidAssignmentException("Assignment to someone exist");
		});
		Optional<User> user = userRepository.findById(assignmentDto.getUserId());
		Optional<Asset> asset = assetRepository.findById(assignmentDto.getAssetId());
		Assignment assignment = new Assignment();
		Long userId = assignmentDto.getUserId();
		Long assetId = assignmentDto.getAssetId();
		assignment.setUser(user.orElseThrow(() -> new InvalidAssignmentException("wrong user id: " + userId)));
		assignment.setAsset(asset.orElseThrow(() -> new InvalidAssignmentException("wrong assignment id: " + assetId)));
		assignment.setStart(LocalDateTime.now());
		return AssignmentMapper.toDto(assignmentRepository.save(assignment));
	}

	@Transactional
	public LocalDateTime finishAssignment(Long assignmentId) {
		Optional<Assignment> assignment = assignmentRepository.findById(assignmentId);
		Assignment assignmentEntity = assignment.orElseThrow(AssignmentNotFoundException::new);
		if (assignmentEntity.getEnd() != null)
			throw new AssignmentAlreadyFinishedException();
		else
			assignmentEntity.setEnd(LocalDateTime.now());
		return assignmentEntity.getEnd();
	}

}
