package pl.javastart.equipy.Exception;

public class InvalidAssignmentException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public InvalidAssignmentException(String message) {
		super(message);
	}

}
