package pl.javastart.equipy.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="User not exist")
public class UserNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 1L;

}
