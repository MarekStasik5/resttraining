package pl.javastart.equipy.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Wrong assignment id")
public class AssignmentNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

}
