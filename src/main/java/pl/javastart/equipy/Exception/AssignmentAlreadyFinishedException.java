package pl.javastart.equipy.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="Assignment finished")
public class AssignmentAlreadyFinishedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

}
