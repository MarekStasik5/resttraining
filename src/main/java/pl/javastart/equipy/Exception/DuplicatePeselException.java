package pl.javastart.equipy.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.CONFLICT, reason="User exist")
public class DuplicatePeselException extends RuntimeException {

	private static final long serialVersionUID = 1L; 

}
