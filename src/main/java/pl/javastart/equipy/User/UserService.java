package pl.javastart.equipy.User;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import pl.javastart.equipy.Exception.DuplicatePeselException;
import pl.javastart.equipy.Exception.UserNotFoundException;

@Service
class UserService {

	private UserReposiotry userRepository;

	UserService(UserReposiotry userRepository) {
		this.userRepository = userRepository;
	}

	List<UserDto> findAll() {
		return userRepository.findAll().stream().map(UserMapper::toDto).collect(Collectors.toList());
	}

	List<UserDto> findByLastName(String lastName) {
		return userRepository.findAllByLastNameContainingIgnoreCase(lastName).stream().map(UserMapper::toDto)
				.collect(Collectors.toList());
	}

	UserDto save(UserDto user) {
		Optional<User> userByPesel = userRepository.findByPesel(user.getPesel());
		userByPesel.ifPresent(u -> {
			throw new DuplicatePeselException();
		});
		User userEntity = UserMapper.toEntity(user);
		User savedUser = userRepository.save(userEntity);
		return UserMapper.toDto(savedUser);
	}

	Optional<UserDto> findById(Long id) {
		return userRepository.findById(id).map(UserMapper::toDto);
	}

	UserDto update(UserDto user) {
		Optional<User> userByPesel = userRepository.findByPesel(user.getPesel());
		userByPesel.ifPresent(u -> {
			if (!u.getId().equals(user.getId()))
				throw new DuplicatePeselException();
		});
		return mapAndSaveUser(user);
	}

	private UserDto mapAndSaveUser(UserDto user) {
		User usedEntity = UserMapper.toEntity(user);
		User savedUser = userRepository.save(usedEntity);
		return UserMapper.toDto(savedUser);
	}

	List<UserAssignmentDto>getUserAssignments(Long userId){
		return userRepository.findById(userId)
				.map(User::getAssignments)
				.orElseThrow(UserNotFoundException::new)
				.stream()
				.map(UserAssignmentMapper::toDto)
				.collect(Collectors.toList());
	}

}
