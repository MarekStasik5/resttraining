package pl.javastart.equipy.User;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserReposiotry extends JpaRepository<User, Long> {
	
	List<User> findAllByLastNameContainingIgnoreCase(String lastName);
	Optional<User>findByPesel(String pesel); 
}
