package pl.javastart.equipy.Category;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

@Service
class CategoryService {
	
	private CategoryRepository categoryReposiotry;
	
	public CategoryService(CategoryRepository categoryReposiotry) {
		this.categoryReposiotry = categoryReposiotry;
	}

	List<String> findAllNames(){
		return categoryReposiotry.findAll()
				.stream()
				.map(Category::getName)
				.collect(Collectors.toList());
	}
}
